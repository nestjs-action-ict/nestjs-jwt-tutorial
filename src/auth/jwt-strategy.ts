import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { AuthService } from './auth.service';
import { UserDTO } from './dto/user.dto';
import { JwtPayload } from './jwt-payload.interface';

/**
 * Questo servizio è l'implementazione custom della PassportStrategy
 *
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  /**
   * Nel costruttore instanzia l'authService ed estrae il token JWT dalla request,
   * usando la stringa secretOrKey per decriptarlo.
   *
   * @param authService
   */
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Non usare MAI in chiaro le chiavi, qui è solo un esempio
      secretOrKey: 'topsecretkey',
    });
  }

  /**
   * il metodo validate a partire da un payload (user e role), ritorna uno UserDTO valido
   * oppure lancia una Unhoutorized exception
   *
   * @param payload
   */
  async validate(payload: JwtPayload): Promise<UserDTO> {
    const { username, role } = payload;
    const user =await this.authService.findMockUser(username);
    if (!user) throw new UnauthorizedException();

    return user;
  }
}
