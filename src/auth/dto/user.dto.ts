import { IsString, MaxLength, MinLength } from 'class-validator';

/**
 * DTO authCredentials: contiene soltanto 2 stringhe username e role
 * Da notare la validazione con @isString, @MinLenght e @MaxLength
 */

export class UserDTO {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @IsString()
  role: string;
}
