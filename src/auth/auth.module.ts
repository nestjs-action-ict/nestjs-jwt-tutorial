import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt-strategy';

@Module({
  /* Vengono importati i moduli che verranno utilizzati all'interno del modulo auth, in particolare PassPortModule e JwtModule.  */
  imports: [
    /* Il modulo PassportModule viene inizializzato con la strategia di default 'jwt' */
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    /* Il modulo JwtModule viene inizializzato con 2 parametri:
    - Il secret che per il momento viene inserito qui ma andrà spostato al più presto
      (lo vedremo quando affronteremo la parametrizzaizione con configMap)
    - Le signOptions che sono valori opzionali da passare al JwtModule; nel nostro caso
      passiamo expiresIn che rappresenta la scadenza del token in millisecondi
     */
    JwtModule.register({
      secret: 'topsecretkey',
      signOptions: {
        expiresIn: 3600,
      },
    }),
  ],
  /* Qui vengono dichiarati i controller del modulo */
  controllers: [AuthController],
  /* Qui vengono dichiarati i providers/servizi del nostro modulo */
  providers: [AuthService, JwtStrategy],
  /* Qui vengono dichiarati gli exports, cioè quello che esportiamo per poterlo usare in altri moduli */
  exports: [JwtStrategy, PassportModule],
})
export class AuthModule {}
