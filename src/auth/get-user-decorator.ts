import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserDTO } from './dto/user.dto';

/**
 *  Questo Decorator prende la richiesta http e restituisce lo UserDTO;
 *  è un metodo di utilità che può essere usato nei controllers per ottenere direttamente lo
 *  user.
 */
export const GetUser = createParamDecorator(
  (data, ctx: ExecutionContext): UserDTO => {
    const req = ctx.switchToHttp().getRequest();
    return req.user;
  },
);
