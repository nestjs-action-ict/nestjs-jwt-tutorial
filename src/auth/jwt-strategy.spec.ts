import  {JwtStrategy} from './jwt-strategy';
import {Test} from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UnauthorizedException } from '@nestjs/common';
import { UserDTO } from './dto/user.dto';

const mockAuthService = ()  =>  ({
    findMockUser : jest.fn()
})

describe('JwtStrategy', () =>  {

    let jwtStrategy;
    let authService;

     /**
     * beforeEach venga chiamato prima di ogni test case, 
     * l'idea è di eseguire ogni test case in modo indipendente con un'ambientazione nuova
     */
    beforeEach(async () =>  {
        /*
          Qui creiamo il modulo di test e compilarlo, Le classi che useremo in test sono specificate come provider,
           in questo caso AuthService e JwtService. Dobbiamo anche fare mock di AuthService,
           per questo usiamo provide: AuthService ,Per specificare la sostituzione del servizio originale
           usiamo useFactory:mockAuthService
        */
        const module = await Test.createTestingModule( {
            providers: [JwtStrategy, 
                {provide: AuthService, useFactory:mockAuthService }]
        }).compile();

        jwtStrategy=await  module.get<JwtStrategy>(JwtStrategy);
        authService=await  module.get<AuthService>(AuthService);
    })
 
    describe('validate', () =>  {
        it('should return UserDTO if validation passes',async () => {
            let payload ={
                username: 'user1',
                role: 'user'
              }
    
              let userDTO = new UserDTO();
              userDTO.username=payload.username;
              
             authService.findMockUser.mockResolvedValue(userDTO);
              expect(await jwtStrategy.validate(payload)).toEqual(userDTO);
              expect(authService.findMockUser).toBeCalledWith(payload.username);
        } )
    
        it('should throw UnauthorizedException if validation fails', () => {
               let payload ={} 
               authService.findMockUser.mockResolvedValue(null);
               expect( jwtStrategy.validate(payload)).rejects.toThrow(UnauthorizedException);
        } )
    }) 
})