import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthCredentialsDTO } from './dto/auth.credentials.dto';
import { UserDTO } from './dto/user.dto';
import { JwtPayload } from './jwt-payload.interface';

/**
 * Il decorator @Injectable sta ad indicare che questo service può essere 'iniettato'
 * nel costruttore dei controller.
 *
 * @See auth.controller.ts
 */
@Injectable()
export class AuthService {
  /**
   * Costruttore, qui instanziamo il nostro servizio JwtService
   *
   * @param jwtService
   */
  constructor(private jwtService: JwtService) {}

  /**
   * Metodo SignIn: Prende in ingresso le nostre credenziali e ritorna un token.
   * Se l'autenticazione fallisce ritorna un UnouthorizedException
   *
   * @param authCredentialsDTO
   *
   * @return accessToken
   *
   * @exception UnauthorizedException
   */
  async signIn(
    authCredentialsDTO: AuthCredentialsDTO,
  ): Promise<{ accessToken: string }> {
    // Chiama il repository, per vedere se l'utente esiste. Nel nostro caso usiamo un servizio Mocked
    const { username, role } = this.validateUserAndPassword(authCredentialsDTO);
    if (!username) throw new UnauthorizedException('Invalid credentials');

    const payload: JwtPayload = { username, role };
    const accessToken = await this.jwtService.sign(payload);
    return { accessToken };
  }

  /**
   * Metodo validateUserAndPassword, prende in ingresso AuthCredentialsDTO
   * restituisce uno UserDTO. Questo metodo convalida utilizzando nome utente e password e restituisce UserDTO
   * @param AuthCredentialsDTO
   *
   * @return UserDTO
   */
  validateUserAndPassword(authCredentialsDTO: AuthCredentialsDTO): UserDTO {
    const  {username,password}=authCredentialsDTO;
    //Qui dovrebbe avvenire la convalida effettiva e se la convalida passa viene restituito l'oggetto utente
    return this.findMockUser(username);
  }


 /**
   * Metodo findMockUser, prende in ingresso username
   * restituisce uno UserDTO. Questo è un metodo mockato, in produzione ci sarà un
   * meccanismo di verifica efficace (db o altro). In caso non trovi niente restituisce null
   *
   * @param username
   *
   * @return UserDTO
   */
  findMockUser(username: string): UserDTO {
    const mockUserNames = ['user1', 'user2'];
    if (mockUserNames.indexOf(username) != -1) {
      const userDTO = new UserDTO();
      userDTO.username = username;
      userDTO.role = 'admin';
      return userDTO;
    }
    //Restituisco oggetto vuoto in caso di mancata autenticazione
    return {username:null, role:null};
  }

  
}
