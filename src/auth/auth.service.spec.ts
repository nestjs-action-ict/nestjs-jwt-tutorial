
import { UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {Test} from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthCredentialsDTO } from './dto/auth.credentials.dto';

const mockJwtService = () =>  ({
    sign: jest.fn()
});


describe('AuthService', () =>  {
    let authService;
    let jwtService;
    
    /**
     * beforeEach venga chiamato prima di ogni test case, 
     * l'idea è di eseguire ogni test case in modo indipendente con un'ambientazione nuova
     */
    beforeEach(async () => {
        /*
          Qui creiamo il modulo di test e compilarlo, Le classi che useremo in test sono specificate come provider,
           in questo caso AuthService e JwtService. Dobbiamo anche fare mock di JwtService,
           per questo usiamo provide : JwtService e useFactory: mockJwtService per specificare la sostituzione del servizio originale
        */
          const module =await Test.createTestingModule({
            providers:[AuthService,
                {provide : JwtService , useFactory : mockJwtService }]
        }).compile();
        authService = await module.get<AuthService>(AuthService);
        jwtService = await module.get<JwtService>(JwtService);
    })
    
    describe('Sign in', () =>  {
        /**
         * Questo caso di test prevede che venga lanciata un'execption se viene tentata una sigin non autorizzata
         */
        it('throws exception if authetication fails',async ()=>  {
            // Qui stiamo chiamando signIn senza impostare il nome utente e la password
            // Ci aspettiamo che venga generata UnauthorizedException
            let authCredentialsDTO= new AuthCredentialsDTO();
            expect(authService.signIn(authCredentialsDTO)).rejects.toThrow(UnauthorizedException);
        })

        /**
         * Questo caso di test verifica che venga creato un token di convalida riuscito
         */
        it('returns token after successful validation',async ()=>  {
            // Qui invochiamo l'accesso con un nome utente e una password validi
            //Usiamo 'user1' che è un utente valido che possiamo trovare in AuthService.findMockUser()
            let authCredentialsDTO= new AuthCredentialsDTO();
            authCredentialsDTO.username ='user1';
            authCredentialsDTO.password ='password1'
            const mockAccessToken ='header.payload.signature';
            jwtService.sign.mockResolvedValue(mockAccessToken);
            const token =await authService.signIn(authCredentialsDTO);

            //Verifichiamo se l'signIn restituisce il token deriso
            expect(token).toEqual( {"accessToken": mockAccessToken});

            //Verifichiamo se è stato utilizzato il payload corretto per richiamare il metodo di sign di jwtService
            let payload = { username :  authCredentialsDTO.username, role :'admin' };
            expect(jwtService.sign).toBeCalledWith(payload);
    
        })
    })
})