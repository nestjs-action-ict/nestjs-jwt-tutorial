import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { AuthModule } from './auth/auth.module';

/* Il decorator @Module dell'applicazione importa gli altri module che verranno
utilizzati all'interno dell'applicazione
 */
@Module({
  imports: [TasksModule, AuthModule],
})
export class AppModule {}
