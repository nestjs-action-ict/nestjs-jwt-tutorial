import { Injectable } from '@nestjs/common';
import { UserDTO } from 'src/auth/dto/user.dto';
import { TaskDTO } from './dto/task-dto';

@Injectable()
export class TasksService {
  getTasks(user: UserDTO): TaskDTO[] {
    // Here call to task repository should be make, for this example we are mocking this call
    return this.mockFindTasksFromDB(user);
  }

  mockFindTasksFromDB(user: UserDTO): TaskDTO[] {
    if (user.username === 'user1') {
      return [
        { title: 'title1', description: 'description1' },
        { title: 'title2', description: 'description2' },
      ];
    }

    if (user.username === 'user2') {
      return [
        { title: 'title3', description: 'description3' },
        { title: 'title4', description: 'description4' },
      ];
    }
    return null;
  }
}
