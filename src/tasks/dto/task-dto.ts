import { IsNotEmpty } from 'class-validator';

/** DTO per rappresentare i task */
export class TaskDTO {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  description: string;
}
