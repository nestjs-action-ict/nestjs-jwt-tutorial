import { Controller, Get, Logger, UseGuards } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user-decorator';
import { UserDTO } from 'src/auth/dto/user.dto';
import { TaskDTO } from './dto/task-dto';

@Controller('tasks')
/**
 * @UseGuards Viene utilizzato per proteggere tutti gli endpoint di questo controller (/tasks). Soltanto
 * gli utenti autorizzati potranno accedere. GLi altri avranno un'eccezione di tipo
 * UnhoutorizedException.
 *
 * E' possibile utilizzare @UseGuards sui singoli endpoint
 *
 */
@UseGuards(AuthGuard())
export class TasksController {
  private logger = new Logger('TaskController');

  constructor(private taskService: TasksService) {}

  @Get()
  getTasks(@GetUser() user: UserDTO): TaskDTO[] {
    return this.taskService.getTasks(user);
  }
}
