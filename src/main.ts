import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  /* Logger è il logger di default di NestJS; Il parametro fornisce il contesto
  al logger: Guardando l'ultima riga di log una volta avviata l'applicazione, noterete:
  -- [Bootstrap] Listening on port 3000 .. --
   */
  const logger = new Logger('Bootstrap');
  /* NestFactory.create è un metodo asincrono che inizializza l'applicazione. In questo momento
  inizializza i moduli e fa il discover degli endpoint
   */
  const app = await NestFactory.create(AppModule);

  const PORT = 3000;
  /* Listen mette il server in ascolto sulla porta selezionata */
  await app.listen(PORT);
  logger.log(`Listening on port ${PORT} ..`);
}

bootstrap();
